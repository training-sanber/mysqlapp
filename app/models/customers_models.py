from os import access
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from config import AccessDb

Base = declarative_base()
engine = create_engine('mysql+mysqlconnector://{0}:{1}@{2}:3306/{3}'.format(AccessDb.USER, AccessDb.PASSWORD, AccessDb.HOSTDB, AccessDb.DATABASE), echo = True) #ganti sesuai kebutuhan
Session = sessionmaker(bind=engine)
session = Session()

class Customers(Base):
   __tablename__ = 'customers'
   userid = Column(Integer, primary_key =  True)
   username = Column(String)
   namaLengkap = Column(String)
   email = Column(String)

   def __init__(self, username:str, namaLengkap:str, email:str):
       self.username = username
       self.namaLengkap = namaLengkap
       self.email = email