from mysql.connector import connect
from config import AccessDb

class database:
    def __init__(self):
        try:
            self.db = connect(host=AccessDb.HOSTDB,
                            database=AccessDb.DATABASE,
                            user=AccessDb.USER,
                            password=AccessDb.PASSWORD,
                            port=AccessDb.PORT
                            )
        except Exception as e:
            print(e)

    def showBorrowByEmail(self, params):
        cursor = self.db.cursor()
        query='''
        select customers.username,borrows.*
        from borrows
        inner join customers on borrows.userid = customers.userid
        where customers.email = "{0}" and borrows.isactive = 1;
        '''.format(params)

        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def insertBorrow(self, **params):
        #column = "isactive"
        #values = 1
        column = ', '.join(list(params.keys()))
        values = tuple(list(params.values()))
        # print(column)
        # print(values)
        cursor = self.db.cursor()
        query='''
        insert into borrows ({0})
        values {1};
        '''.format(column, values)
        cursor.execute(query)

    def updateBorrow(self, **params):
        borrowid = params["borrowid"]
        #values = self.restuctureParams(**params)
        cursor = self.db.cursor()
        print(borrowid)
        query='''
        update borrows
        set isactive = 0
        where borrowid = {0};
        '''.format(borrowid)
        cursor.execute(query)

    def showUserByEmail(self, params):
        cursor = self.db.cursor()
        query='''
        select *
        from customers
        where email = "{0}";
        '''.format(params)
        cursor.execute(query)
        result = cursor.fetchone()
        return result

    def dataCommit(self):
        self.db.commit()
