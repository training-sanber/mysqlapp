from app import app
from app.controllers import customers_controllers
from flask import Blueprint, request

customers_blueprint = Blueprint("customers_routers", __name__)

@app.route('/gettoken/', methods=['POST'])
def requestToken(**params):
    params = request.json
    return customers_controllers.token(params)

@app.route('/users/', methods=['GET'])
def showUsers_():
    return customers_controllers.showUsers()

@app.route('/users/<int:pk>/', methods=['GET'])
def showUserId(pk):
    return customers_controllers.showUserById(pk)

@app.route('/users/insert/', methods=['POST'])
def insertUser_():
    params = request.json
    return  customers_controllers.insertUser(**params)

@app.route('/users/<int:pk>/update/', methods=['PUT'])
def updateUserById_(pk, **params):
    params = request.json
    return customers_controllers.updateUserById(pk, **params)

@app.route('/users/<int:pk>/delete/', methods=['DELETE'])
def deleteUserById_(pk):
    return customers_controllers.deleteUserById(pk)

