from app import app
from app.controllers import borrows_controllers
from flask import Blueprint, request

borrows_blueprint = Blueprint("borrows_routers", __name__)

@app.route("/borrows/", methods=["GET"])
def showBorrow():
    return borrows_controllers.show()

@app.route("/borrows/insert/", methods=["POST"])
def insertBorrow():
    params = request.json
    return borrows_controllers.insert(**params)

@app.route("/borrows/status/", methods=["PUT"])
def statusBorrow():
    params = request.json
    return borrows_controllers.changeStatus(**params)


