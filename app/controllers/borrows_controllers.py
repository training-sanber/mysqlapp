from app.models.borrows_models import database
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime, requests
from config import Link

mysqldb = database()

@jwt_required()
def show():
    params = get_jwt_identity()
    dbresult = mysqldb.showBorrowByEmail(params)
    #print (params)
    #return params
    result = []
    if dbresult is not None:
        for items in dbresult:
            id = json.dumps({"id":items[4]})
            bookdetail = getBookById(id)
            user = {
                "username": items[0],
                "borrowid": items[1],
                "borrowdate": items[2],
                "bookid":  items[4],
                "bookname": items[5],
                "author": bookdetail["pengarang"],
                "releaseyear": bookdetail["tahunterbit"],
                "genre": bookdetail["genre"]
            }
            result.append(user)
    return jsonify(result)

@jwt_required()
def insert(**params):
    token = get_jwt_identity()
    userid = mysqldb.showUserByEmail(token)[0]
    borrowdate = datetime.datetime.now().isoformat()
    id = json.dumps({"id":params["bookid"]})
    bookname = getBookById(id)["nama"]
    params.update(
        {
            "bookid": params["bookid"],
            "userid": userid,
            "borrowdate": borrowdate,
            "bookname": bookname,
            "isactive":1
        }
    )
    mysqldb.insertBorrow(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})
    

@jwt_required()
def changeStatus(**params):
    mysqldb.updateBorrow(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

def getBookById(data):
    book_data = requests.post(url=Link.LINK, data=data)
    return book_data.json()



