from sqlalchemy import and_
from app.models.customers_models import Customers, session as result
from flask_jwt_extended import *
import datetime
from flask import jsonify

@jwt_required()
def showUsers():
    final = []
    show = result.query(Customers).all()
    for row in show:
        user = {
            "userid" : row.userid,
            "username" : row.username,
            "namaLengkap" : row.namaLengkap,
            "email" : row.email
        } 
        final.append(user)
        result.commit()
    return jsonify(final)

@jwt_required()
def showUserById(pk):
    final = []
    show = result.query(Customers).filter(Customers.userid==pk)
    for row in show:
        user = {
            "userid" : row.userid,
            "username" : row.username,
            "namaLengkap" : row.namaLengkap,
            "email" : row.email
        } 
        final.append(user)
        result.commit()
    return jsonify(final)

@jwt_required()
def insertUser(**params):
    try:
        data_ = Customers(params['username'], params['namaLengkap'], params['email'])
        result.add(data_)
        result.commit()
        data = {
                "status":"Success"
                }
        return data
    except:
        result.rollback()
        data = {
                "status":"Failed",
                "keterangan":"data sudah double atau format json kurang tepat"
                }
        return data

@jwt_required()
def updateUserById(pk, **params):
    try:
        update_ = result.query(Customers).filter(Customers.userid==pk)
        update_.update(params)
        data = {
                "status":"Success"
                }
        return data
    except:
        data = {
                "status":"Failed",
                "keterangan":"data sudah double atau format json kurang tepat"
                }
        return data

#@jwt_required()
def deleteUserById(pk):
    try: 
        delete = result.query(Customers).filter(Customers.userid==pk).one()
        result.delete(delete)
        result.commit()
        data = {
                "status":"Success"
                }
        return data
    except:
        data = {
                "status":"Failed",
                "keterangan":"ID tidak ditemukan, mohon cek kembali IDnya"
        }
        return data

def token(params):
    try:
        row = result.query(Customers).filter(and_(Customers.email==params['email'], Customers.username==params['username']))
        if row is not None:
            for se in row:
                set = {
                    "username":se.username,
                    "email":se.email
                }
                expires = datetime.timedelta(minutes=1000)
                expires_refresh = datetime.timedelta(minutes=1)
                access_token = create_access_token(se.email, fresh=True, expires_delta=expires)

                data = {
                    "data": set,
                    "token_access": access_token
                }
        return data
    
    except:
        data = {
                "status":"Failed",
                "keterangan":"Email / username tidak terdaftar atau format salah"
            }
        return data

def showUserByEmail(email):
    final = []
    show = result.query(Customers).filter(Customers.email==email)
    for row in show:
        user = {
            "userid" : row.userid,
            "username" : row.username,
            "namaLengkap" : row.namaLengkap,
            "email" : row.email
        } 
        final.append(user)
        result.commit()
    return final