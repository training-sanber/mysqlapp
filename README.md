# mysqlaapp

ada 3 Microservice 1 dengan FastApi+mongodb dan 2 menggunakan Flask+mysql yang bertujuan untuk membuat Aplikasi Perpustakaan

ini adalah Microservice menggunakan Flask+mysql

Microservice Ini Untuk menampilkan Daftar Customers serta peminjaman.

Microservice ini menggunakan Python 3.9.6, Flask dan Jwt untuk authorization
Disini menggunakan database Mysql, untuk mengganti username,password,host serta port
ke bagian mysqlapp/app/models/customers_models.py untuk customers
ke bagian mysqlapp/app/models/borrows_models.py untuk peminjaman

## Instalasi

Untuk Instalasi silahkan buat Virtualenv, unduh microservice lalu install requirement
```
git clone https://gitlab.com/training-sanber/mysqlapp.git

cd mysqlapp

pip install -r requirement.txt
```

Disini menggunakan database Mysql, untuk mengganti username,password,host serta port
ke bagian mysqlapp/app/models/customers_models.py untuk customers
ke bagian mysqlapp/app/models/borrows_models.py untuk peminjaman

file database yaitu "database.sql"

Selanjutnya ganti user,password serta credential lainnya di 
```
.flaskenv
```
Cara menjalankan
```
python main.py
```
Rest API yang tersedia ada 2 Tipe yaitu untuk Customer/User dan Borrow atau peminjaman dan cara penggunaan

## Customer/User 

### 1. Untuk meminta token

localhost:5000/gettoken/

Cara menggunakan dengan method "POST"
dengan format JSON :

```
{
    "username":"user",
    "email":"email"

}
```

### 2. Untuk menampilkan User berdasarkan ID 

```
localhost:5000/users/"id"/
```

Cara menggunakan dengan method "GET" dan authorization "Bearer Token".
minta token menggunakan cara 1.

### 3. Untuk menambahkan User

```
localhost:5000/insert/
```

cara menggunakan dengan method "POST", authorization "Bearer Token" serta
dengan format JSON :

```
{
    "username":"user",
    "namaLengkap":"namalengkap"
    "email":"email"
}
```

### 4. Untuk menampilkan semua buku

```
localhost:5000/users/
```

cara menggunakan dengan method "GET" dan authorization "Bearer Token".

### 5. Untuk update data user

```
localhost:5000/users/"id"/update/
```

cara menggunakan dengan method "PUT" dan authorization "Bearer Token" serta
dengan format JSON :
```
{
    "username":"userupdate",
    "namaLengkap":"namalengkapupdate"
    "email":"emailupdate"

}
```

### 6. Untuk hapus User dengan ID

```
localhost:5000/users/"id"/delete/
```

cara menggunakan dengan method "DELETE" dan authorization "Bearer Token".

### 7. Untuk tambah User

```
localhost:5000/users/"insert"/
```

cara menggunakan dengan method "POST" dan authorization "Bearer Token" dengan 
format JSON :
```
{
    "username":"userbaru",
    "namaLengkap":"namalengkapbaru"
    "email":"emailbaru"

}
```

## Borrow

### 1. Untuk melihat data borrow atau peminjaman dari user tersebut

```
localhost:5000/borrows/
```

cara menggunakan dengan method "GET" dan authorization "Bearer Token". Masing masing
user hanya bisa mengakses data peminjaman mereka saja.

### 2. Untuk Menambah Peminjaman buku

```
localhost:5000/borrows/insert/
```

cara menggunakan dengan method "PUT" dan authorization "Bearer Token" serta dengan format json :
```
{
    "bookid":"idbuku"

}
```

### 3. Untuk mengembalikan buku

localhost:5000/borrows/status/

cara menggunakan dengan method "POST" dan authorization "Bearer Token" serta dengan format json
```
{
    "borrowid":"borrowid"

}
```











