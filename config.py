import os

base_dir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    JSON_SORT_KEYS = False
    JWT_SECRET_KEY = str(os.environ.get("JWT_SECRET"))

class AccessDb(object):
    HOSTDB = str(os.environ.get("HOSTDB"))
    PORT = str(os.environ.get("PORT"))
    DATABASE = str(os.environ.get("DATABASE"))
    USER = str(os.environ.get("USER"))
    PASSWORD = str(os.environ.get("PASSWORD"))

class Link(object):
    LINK = str(os.environ.get("LINK"))

class Main(object):
    DEBUG = os.environ.get("DEBUG")
    HOST = os.environ.get("HOST")
