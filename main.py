from app import app
from config import Main

if __name__ == "__main__":
    app.run(debug=Main.DEBUG, host=Main.HOST)
